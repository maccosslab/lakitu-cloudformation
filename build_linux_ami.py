#!/usr/bin/env python

"""
build_linux_ami.py
Generate an AMI for the lakitu linux cluster.
Reads in an existing ECS AMI (pull from http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html)
 and builds a Lakitu AMI from it. The Lakitu AMI is similar to the default ECS AMI, just with some modification of the
 ECS configuration and a larger EBS volume attached at the root directory for container storage.
"""

import boto3
import logging
import sys
import argparse
from time import sleep

logger = logging.getLogger('build_linux_ami')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
logger.addHandler(ch)

ec2_client = boto3.client('ec2')
ec2 = boto3.resource('ec2')


def create_parser():
    parser = argparse.ArgumentParser(
        description="Create an AMI optimized for the Lakitu pipeline from an existing Linux ECS instance. "
                    "Recommended to use one of the base ECS AMIs from Amazon: "
                    "http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html)",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--storage-size', default=250,
                        help="Root volume size to mount on the instance by default (in GiB)")
    parser.add_argument('--storage-type', default='gp2',
                        help="Root EBS volume storage type")
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="If a lakitu optimized image is already found, deregister and overwrite it")
    parser.add_argument('template_ami', help="AMI number to build the optimized AMI from (ex. ami-20ff515a)")
    return parser


instance_user_data = """#!/bin/bash

echo ECS_IMAGE_CLEANUP_INTERVAL=10m >> /etc/ecs/ecs.config
echo ECS_IMAGE_MINIMUM_CLEANUP_AGE=1m >> /etc/ecs/ecs.config
echo ECS_NUM_IMAGES_DELETE_PER_CYCLE=100 >> /etc/ecs/ecs.config
"""


def main():
    parser = create_parser()
    args = parser.parse_args()
    image_name = "lakitu_{}_{}_{}".format(args.storage_size, args.storage_type, args.template_ami)
    # check if image exists
    im_search = ec2_client.describe_images(
        Filters=[
            {'Name': 'name', 'Values': [image_name]},
            {'Name': 'is-public', 'Values': ['false']}])['Images']
    if len(im_search) > 0:
        logger.info("Image with name {} has already been created. AMI is: {}".format(image_name,
                                                                                     im_search[0]['ImageId']))
        if args.overwrite:
            # deregister existing images
            for im in im_search:
                logger.info("Overwrite flag is enabled, deregistering existing image {}".format(im['ImageId']))
                image = ec2.Image(im['ImageId'])
                image.deregister()
                sleep(5)
        else:
            return

    logger.info("Deploying template instance")
    instance = ec2.create_instances(
        ImageId=args.template_ami,
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',
        BlockDeviceMappings=[
            {
                "DeviceName": "/dev/xvdcz",
                "Ebs":
                    {"DeleteOnTermination": True,
                     "VolumeSize": args.storage_size,
                     "VolumeType": args.storage_type}
            }
        ],
        UserData=instance_user_data
        )[0]
    logger.info("Deployed instance id: {}".format(instance.id))
    logger.info("Waiting for deployed instance to initialize")
    instance.wait_until_running()
    logger.info("Instance is in running state")
    logger.info("Creating image")

    image = instance.create_image(
        Description="A customized Lakitu ECS image",
        Name=image_name)
    image.wait_until_exists()
    logger.info("Creating new image named {} with AMI {}".format(image_name, image.image_id))
    logger.info("Waiting for image creation to complete")
    waiter = ec2_client.get_waiter('image_available')
    waiter.wait(ImageIds=[image.image_id])
    logger.info("Image creation successful")

    logger.info("Terminating template EC2 instance")
    instance.terminate()
    instance.wait_until_terminated()
    logger.info("Instance successfully terminated.")
    logger.info("Great success!")


if __name__ == '__main__':
    main()
