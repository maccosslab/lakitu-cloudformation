Description: >
    This template deploys a VPC for Lakitu with two public subnets -- one for an SSH bastion, another for a NAT server.
    The private subnet will house the Windows and Linux ECS clusters which access the outside net (to grab containers) 
     and contact the ECS service via the NAT server.

Parameters:

    EnvironmentName:
        Description: An environment name that will be prefixed to resource names
        Type: String

    VpcCIDR: 
        Description: Please enter the IP range (CIDR notation) for this VPC
        Type: String
        Default: 10.0.0.0/16

    PublicSubnetBastionCIDR:
        Description: Please enter the IP range (CIDR notation) for the SSH bastion public subnet
        Type: String
        Default: 10.0.1.0/24

    PublicSubnetNatCIDR:
        Description: Please enter the IP range (CIDR notation) for the NAT server public subnet
        Type: String
        Default: 10.0.2.0/24

    PrivateSubnetClusterCIDR:
        Description: Please enter the IP range (CIDR notation) for the private cluster subnet
        Type: String
        Default: 10.0.3.0/24

Resources:

    VPC: 
        Type: AWS::EC2::VPC
        Properties:
            CidrBlock: !Ref VpcCIDR
            Tags: 
                - Key: Name 
                  Value: !Ref EnvironmentName
            
    InternetGateway:
        Type: AWS::EC2::InternetGateway
        Properties:
            Tags:
                - Key: Name
                  Value: !Ref EnvironmentName
            
    InternetGatewayAttachment:
        Type: AWS::EC2::VPCGatewayAttachment
        Properties:
            InternetGatewayId: !Ref InternetGateway
            VpcId: !Ref VPC

    PublicSubnetBastion: 
        Type: AWS::EC2::Subnet
        Properties:
            VpcId: !Ref VPC
            AvailabilityZone: !Select [ 0, !GetAZs ]
            CidrBlock: !Ref PublicSubnetBastionCIDR
            MapPublicIpOnLaunch: true
            Tags: 
                - Key: Name 
                  Value: !Sub ${EnvironmentName} Public Subnet (SSH Bastion)

    PublicSubnetNat: 
        Type: AWS::EC2::Subnet
        Properties:
            VpcId: !Ref VPC
            AvailabilityZone: !Select [ 0, !GetAZs ]
            CidrBlock: !Ref PublicSubnetNatCIDR
            MapPublicIpOnLaunch: true
            Tags: 
                - Key: Name 
                  Value: !Sub ${EnvironmentName} Public Subnet (NAT Server)
 
    PrivateSubnetCluster: 
        Type: AWS::EC2::Subnet
        Properties:
            VpcId: !Ref VPC
            AvailabilityZone: !Select [ 0, !GetAZs ]
            CidrBlock: !Ref PrivateSubnetClusterCIDR
            MapPublicIpOnLaunch: false
            Tags: 
                - Key: Name 
                  Value: !Sub ${EnvironmentName} Private Subnet (ECS Clusters)

    NatGatewayEIP:
        Type: AWS::EC2::EIP
        DependsOn: InternetGatewayAttachment
        Properties: 
            Domain: vpc

    NatGateway: 
        Type: AWS::EC2::NatGateway
        Properties: 
            AllocationId: !GetAtt NatGatewayEIP.AllocationId
            SubnetId: !Ref PublicSubnetNat

    PublicRouteTable:
        Type: AWS::EC2::RouteTable
        Properties: 
            VpcId: !Ref VPC
            Tags: 
                - Key: Name 
                  Value: !Sub ${EnvironmentName} Public Routes

    DefaultPublicRoute: 
        Type: AWS::EC2::Route
        DependsOn: InternetGatewayAttachment
        Properties: 
            RouteTableId: !Ref PublicRouteTable
            DestinationCidrBlock: 0.0.0.0/0
            GatewayId: !Ref InternetGateway

    PublicSubnetBastionRouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
            RouteTableId: !Ref PublicRouteTable
            SubnetId: !Ref PublicSubnetBastion

    PublicSubnetNatRouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
            RouteTableId: !Ref PublicRouteTable
            SubnetId: !Ref PublicSubnetNat
    

    PrivateRouteTableCluster:
        Type: AWS::EC2::RouteTable
        Properties: 
            VpcId: !Ref VPC
            Tags: 
                - Key: Name 
                  Value: !Sub ${EnvironmentName} Private Routes (ECS cluster)

    DefaultPrivateRoute:
        Type: AWS::EC2::Route
        Properties:
            RouteTableId: !Ref PrivateRouteTableCluster
            DestinationCidrBlock: 0.0.0.0/0
            NatGatewayId: !Ref NatGateway

    PrivateSubnetClusterRouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
            RouteTableId: !Ref PrivateRouteTableCluster
            SubnetId: !Ref PrivateSubnetCluster

Outputs: 

    VPC: 
        Description: A reference to the created VPC
        Value: !Ref VPC

    PublicSubnets:
        Description: A list of the public subnets
        Value: !Join [ ",", [ !Ref PublicSubnetBastion, !Ref PublicSubnetNat ]]

    PrivateSubnets:
        Description: A list of the private subnets
        Value: !Join [ ",", [ !Ref PrivateSubnetCluster]]

    PublicSubnet1:
        Description: A reference to the SSH Bastion subnet
        Value: !Ref PublicSubnetBastion

    PublicSubnet2: 
        Description: A reference to the NAT server public subnet
        Value: !Ref PublicSubnetNat

    PrivateSubnetCluster:
        Description: A reference to the private subnet for the ECS clusters
        Value: !Ref PrivateSubnetCluster
