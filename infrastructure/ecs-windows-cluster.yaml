Description: >
    This template deploys an ECS windows cluster to the provided VPC and subnets
    using an Auto Scaling Group

Parameters:

    EnvironmentName:
        Description: An environment name that will be prefixed to resource names
        Type: String

    InstanceType: 
        Description: Which instance type should we use to build the ECS cluster?
        Type: String
        Default: c4.xlarge

    ClusterSize:
        Description: How many ECS hosts do you want to initially deploy?
        Type: Number
        Default: 4

    MinClusterSize:
        Description: What is the minimum number of instances for the Windows cluster?
        Type: Number
        Default: 0

    MaxClusterSize:
        Description: What is the maximum number of instances for the Windows cluster?
        Type: Number
        Default: 128

    VolumeSize:
        Description: What size should the data volumes be attached to each ECS cluster node?
        Type: String
        Default: '100'

    VPC:
        Description: Choose which VPC this ECS cluster should be deployed to
        Type: AWS::EC2::VPC::Id

    Subnets:
        Description: Choose which subnets this ECS cluster should be deployed to
        Type: List<AWS::EC2::Subnet::Id>

    SecurityGroup:
        Description: Select the Security Group to use for the ECS cluster hosts
        Type: AWS::EC2::SecurityGroup::Id

    DataBucketName:
        Description: Name of S3 bucket (e.g. maccosslab-lakitudata) created for storing data processed by lakitu
        ConstraintDescription: Must be the name of an existing S3 bucket.
        Type: String

    ECSRole:
        Description: ECS IAM Role name for container instances
        Type: String

Mappings:

    # These are the latest ECS optimized AMIs as of December 2017:
    #
    #   Windows_Server-2016-English-Full-ECS_Optimized-2017.11.24
    #   ECS agent:
    #   Docker:       17.06.2-ee-5
    #   ecs-init:
    #
    # You can find the latest available on this page of our documentation:
    # http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_Windows_getting_started.html
    # (note the AMI identifier is region specific)

    AWSRegionToAMI:
        us-west-2:
            AMI: ami-0b60bc73
        us-west-1:
            AMI: ami-a8003bc8
        us-east-2:
            AMI: ami-b19fb1d4
        us-east-1:
            AMI: ami-9f1182e5
        eu-west-2:
            AMI: ami-3da4bb59
        eu-west-1:
            AMI: ami-94d360ed
        eu-central-1:
            AMI: ami-b4ed61db
        ca-central-1:
            AMI: ami-2859e24c
        ap-southeast-2:
            AMI: ami-918075f3
        ap-southeast-1:
            AMI: ami-ec32618f
        ap-northeast-2:
            AMI: ami-bb3691d5
        ap-northeast-1:
            AMI: ami-5ed66f38

Resources:

    ECSCluster:
        Type: AWS::ECS::Cluster
        Properties:
            ClusterName: !Ref EnvironmentName

    ECSAutoScalingGroup:
        Type: AWS::AutoScaling::AutoScalingGroup
        Properties: 
            VPCZoneIdentifier: !Ref Subnets
            LaunchConfigurationName: !Ref ECSLaunchConfiguration
            MinSize: !Ref MinClusterSize
            MaxSize: !Ref MaxClusterSize
            DesiredCapacity: !Ref ClusterSize
            Tags: 
                - Key: Name
                  Value: !Sub ${EnvironmentName} ECS host
                  PropagateAtLaunch: 'true'
        CreationPolicy:
            ResourceSignal: 
                Timeout: PT20M
        UpdatePolicy:
            AutoScalingRollingUpdate:
                MinInstancesInService: '1'
                MaxBatchSize: '1'
                PauseTime: PT20M
                WaitOnResourceSignals: 'true'

    ECSLaunchConfiguration:
        Type: AWS::AutoScaling::LaunchConfiguration
        Properties:
            ImageId:  !FindInMap [AWSRegionToAMI, !Ref "AWS::Region", AMI]
            InstanceType: !Ref InstanceType
            BlockDeviceMappings:
                - DeviceName: "/dev/sda1"
                  Ebs:
                      VolumeSize: !Ref VolumeSize
                      VolumeType: gp2
            SecurityGroups:
                - !Ref SecurityGroup
            IamInstanceProfile: !Ref ECSInstanceProfile
            UserData:
                "Fn::Base64": !Sub |
                    <script>
                    setx ECS_AVAILABLE_LOGGING_DRIVERS "[\"json-file\",\"awslogs\",\"none\"]" /M
                    cfn-init.exe -v -s ${AWS::StackId} -r ECSLaunchConfiguration --region ${AWS::Region}
                    cfn-signal.exe -e %ERRORLEVEL% --stack ${AWS::StackName} --resource ECSAutoScalingGroup --region ${AWS::Region}
                    </script>

        Metadata:
            AWS::CloudFormation::Init:
                config:
                    commands:
                        01_import_powershell_module:
                            command: !Sub powershell.exe -Command Import-Module ECSTools
                        02_add_instance_to_cluster:
                            command: !Sub powershell.exe -Command Initialize-ECSAgent -Cluster ${ECSCluster} -EnableTaskIAMRole
                    files:
                        c:\cfn\cfn-hup.conf:
                            content: !Join ['', ['[main]
                                    ', stack=, !Ref 'AWS::StackId', '
                                    ', region=, !Ref 'AWS::Region', '
                                    ']]
                        c:\cfn\hooks.d\cfn-auto-reloader.conf:
                            content: !Join ['', ['[cfn-auto-reloader-hook]
                                    ', 'triggers=post.update
                                    ', 'path=Resources.ECSLaunchConfiguration.Metadata.AWS::CloudFormation::Init
                                    ', 'action=cfn-init.exe -v -s ', !Ref 'AWS::StackId', ' -r ECSLaunchConfiguration',
                                    ' --region ', !Ref 'AWS::Region', '
                                    ']]
                    services:
                        windows:
                            cfn-hup:
                                enabled: 'true'
                                ensureRunning: 'true'
                                files:
                                    - c:\cfn\cfn-hup.conf
                                    - c:\etc\cfn\hooks.d\cfn-auto-reloader.conf

    ECSInstanceProfile:
        Type: AWS::IAM::InstanceProfile
        Properties:
            Path: /
            Roles: 
                - !Ref ECSRole

    ScaleDownPolicy:
        Type: AWS::AutoScaling::ScalingPolicy
        Properties:
            AdjustmentType: PercentChangeInCapacity
            AutoScalingGroupName: !Ref ECSAutoScalingGroup
            PolicyType: StepScaling
            StepAdjustments:
              - MetricIntervalUpperBound: 0
                MetricIntervalLowerBound: -5
                ScalingAdjustment: -10
              - MetricIntervalUpperBound: -5
                MetricIntervalLowerBound: -10
                ScalingAdjustment: -25
              - MetricIntervalUpperBound: -10
                ScalingAdjustment: -100

    CPUAlarmLow:
        Type: AWS::CloudWatch::Alarm
        Properties:
            AlarmDescription: Cluster CPU reservation low
            MetricName: CPUReservation
            Namespace: AWS/ECS
            Statistic: Maximum
            Period: 120
            EvaluationPeriods: 30
            Threshold: 10
            AlarmActions:
            - !Ref ScaleDownPolicy
            Dimensions:
            - Name: ClusterName
              Value: !Ref ECSCluster
            ComparisonOperator: LessThanOrEqualToThreshold


Outputs:

    Cluster:
        Description: A reference to the ECS cluster
        Value: !Ref ECSCluster

    AutoScalingGroup:
        Description: The ECS cluster auto scaling group name
        Value: !Ref ECSAutoScalingGroup
