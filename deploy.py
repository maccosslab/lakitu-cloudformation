#!/usr/bin/env python

"""
Deploy a Lakitu cluster on the cloud
"""

import argparse
import boto3
from botocore.exceptions import WaiterError
import getpass
import logging
import sys
import random
import string

logger = logging.getLogger('deploy')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
logger.addHandler(ch)

# Amazon ECS-Optimized AMIs
# These are frequently updated by Amazon, may replace values here with update ones from this page:
# https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
# Future improvement is to search available public images automatically for the proper ECS AMI

amazon_linux_ecs_images = {
    'us-east-1':	'ami-28456852',
    'us-west-2':	'ami-decc7fa6',
    'us-west-1':	'ami-74262414',
    'eu-west-3':	'ami-9aef59e7',
    'eu-west-2':	'ami-67cbd003',
    'eu-west-1':	'ami-1d46df64',
    'eu-central-1':	'ami-509a053f',
    'ap-northeast-2':	'ami-c212b2ac',
    'ap-northeast-1':	'ami-872c4ae1',
    'ap-southeast-2':	'ami-58bb443a',
    'ap-southeast-1':	'ami-910d72ed',
    'ca-central-1':	'ami-435bde27',
    'ap-south-1':	'ami-00491f6f',
    'sa-east-1':	'ami-af521fc3'
}


teardown_template = """#!/usr/bin/env python

import boto3
import logging
import sys

logger = logging.getLogger('teardown')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
logger.addHandler(ch)

cf_client = boto3.client('cloudformation', region_name='{region_name}')
cloudformation = boto3.resource('cloudformation', region_name='{region_name}')

# tear down stack
logger.info("Tearing down CloudFormation stack {stack_name}")
cf_client.delete_stack(StackName='{stack_name}')
waiter = cf_client.get_waiter('stack_delete_complete')
waiter.config.delay = 5
waiter.config.max_attempts = 240
waiter.wait(StackName='{stack_name}')

logger.info("Tear down complete")
"""

instance_user_data = """#!/bin/bash

echo ECS_IMAGE_CLEANUP_INTERVAL=10m >> /etc/ecs/ecs.config
echo ECS_IMAGE_MINIMUM_CLEANUP_AGE=1m >> /etc/ecs/ecs.config
echo ECS_NUM_IMAGES_DELETE_PER_CYCLE=100 >> /etc/ecs/ecs.config
echo ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=1m >> /etc/ecs/ecs.config
"""


def create_parser():
    parser = argparse.ArgumentParser(
        description="Deploy a Lakitu cluster on the cloud. Requires AWS credentials to be discoverable. See "
                    "http://boto3.readthedocs.io/en/latest/guide/configuration.html.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--master-cloudform', default="https://s3.amazonaws.com/jegertso.cloudformation/cloudformation_templates/lakitu/master.yaml",
                        help="Location on s3 of master CloudFormation script for Lakitu infrastructure")
    parser.add_argument('--use-existing-image', default=False, action='store_true',
                        help="If a Lakitu optimized AMI with the specified storage size and type already exists, "
                             "skip image creation and use the existing one. If this flag is not set, a new image is "
                             "created.")
    parser.add_argument("--storage-size", default=250,
                        help="Size of storage to attach to each cluster instance in GiB)")
    parser.add_argument('--storage-type', default='gp2',
                        help="EBS storage type to attach to each cluster instance")
    parser.add_argument('--linux-min-cores', default=0, type=int,
                        help="The minimum number of cores to allow the linux cluster to scale down to")
    parser.add_argument('--linux-max-cores', default=512, type=int,
                        help="The maximum number of cores to allow the linux cluster to scale up to")
    parser.add_argument('--linux-start-cores', default=16, type=int,
                        help="The number of cores to start the cluster with")

    parser.add_argument('--windows-min-instances', default=0, type=int,
                        help="The minimum number of instances to allow the windows cluster to scale down to")
    parser.add_argument('--windows-max-instances', default=128, type=int,
                        help="The maximum number of instances to allow the windows cluster to scale up to")
    parser.add_argument('--windows-start-instances', default=4, type=int,
                        help="The number of instances to start the windows cluster with")
    parser.add_argument('--windows-instance-type', default="c4.xlarge",
                        help="The EC2 instance type to use for the Windows cluster")

    parser.add_argument("--stack-name", default="lakitu-{}".format(getpass.getuser()),
                        help="Name that will be assigned to the cloudformation stacks and resources created. "
                             "Cannot be the name of an existing stack")
    parser.add_argument("--s3-data-bucket", default="maccosslab-lakitudata",
                        help="The S3 databucket (must already exist) to be used by the lakitu cluster for storage."
                             " Most likely, you need to change this parameter to an s3 bucket you have access to")
    parser.add_argument('--disable-rollback', default=False, action="store_true",
                        help="Use this option to disable deletion of AWS resources if they fail to create. Useful"
                             " for development.")
    parser.add_argument("--no-teardown", default=False, action="store_true",
                        help="Normally, a python script to tear down the infrastructure is written to the current "
                             "directory as teardown-<stack-prefix>.py. Use this option to disable this output.")
    parser.add_argument("--region-name", default="us-east-1",
                        help="Region to build AWS infrastructure in. Note that some regions may not support all "
                             "features necessary for running Lakitu. It is recommended to use the default.")
    return parser


def wrap_ecs_image(template_ami, name_prefix='lakitu', ec2_client=None, ec2_r=None,
                   storage_size=250, storage_type='gp2', use_existing=False):
    """
    Create an AMI optimized for the Lakitu pipeline from an existing Linux ECS instance.
    The wrapped AMI has the specified storage attached and
    Recommended to use one of the base ECS AMIs from Amazon:
    http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html)
    :param template_ami: ECS ami to start with
    :param name_prefix: prefix to name the instance by, instances will be named based on the prefix
                        and specified parameters, for example: lakitu_250_gp2_ami-28456852
    :param ec2_client: an ec2 client to use for example boto3.client('ec2')
    :param ec2_r: an ec2 resource to use
    :param storage_size: size of storage volume for new AMI
    :param storage_type: type of storage volume (https://aws.amazon.com/ebs/details/)
    :param use_existing: if an existing AMI is found, use it
    :return: ID of newly created AMI
    """
    if ec2_client is None:
        ec2_client = boto3.client('ec2')
    if ec2_r is None:
        ec2_r = boto3.resource('ec2')

    image_name = "{}_{}_{}_{}".format(name_prefix, storage_size, storage_type, template_ami)
    # check if image exists
    im_search = ec2_client.describe_images(
        Filters=[
            {'Name': 'name', 'Values': [image_name]},
            {'Name': 'is-public', 'Values': ['false']}])['Images']
    if len(im_search) > 0:
        logger.info("Image with name {} has already been created. AMI is: {}".format(image_name,
                                                                                     im_search[0]['ImageId']))
        if use_existing:
            logger.info("Using existing image")
            return im_search[0]['ImageId']
        else:
            image_name += "_{}".format(''.join(random.sample(string.ascii_lowercase, 8)))
            logger.info("Creating a new image with name {}".format(image_name))

    logger.info("Deploying template instance for image creation")
    instance = ec2_r.create_instances(
        ImageId=template_ami,
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',
        BlockDeviceMappings=[
            {
                "DeviceName": "/dev/xvdcz",
                "Ebs":
                    {"DeleteOnTermination": True,
                     "VolumeSize": storage_size,
                     "VolumeType": storage_type}
            }
        ],
        UserData=instance_user_data
        )[0]
    logger.info("Deployed instance id: {}".format(instance.id))
    logger.info("Waiting for deployed instance to initialize")
    instance.wait_until_running()
    logger.info("Instance is in running state")
    logger.info("Creating image")

    image = instance.create_image(
        Description="A customized Lakitu ECS image",
        Name=image_name)
    image.wait_until_exists()
    logger.info("Creating new image named {} with AMI {}".format(image_name, image.image_id))
    logger.info("Waiting for image creation to complete")
    waiter = ec2_client.get_waiter('image_available')
    waiter.wait(ImageIds=[image.image_id])
    logger.info("Image creation successful")

    logger.info("Terminating template EC2 instance")
    instance.terminate()
    instance.wait_until_terminated()
    logger.info("Instance successfully terminated.")
    logger.info("Great success!")
    return image.image_id


def main():
    parser = create_parser()
    args = parser.parse_args()

    # set up the AWS clients
    cf_client = boto3.client('cloudformation', region_name=args.region_name)
    cloudformation = boto3.resource('cloudformation', region_name=args.region_name)

    ec2_client = boto3.client('ec2', region_name=args.region_name)
    ec2 = boto3.resource('ec2', region_name=args.region_name)

    # build custom AMIs for the Linux batch cluster
    linux_ami = wrap_ecs_image(template_ami=amazon_linux_ecs_images[args.region_name],
                               ec2_client=ec2_client,
                               ec2_r=ec2,
                               storage_size=args.storage_size,
                               storage_type=args.storage_type,
                               use_existing=args.use_existing_image)

    # build the batch stack
    logger.info("Building resources in region: {}".format(args.region_name))
    logger.info("Creating CloudFormation stack {}".format(args.stack_name))
    params = {'DataBucketName': args.s3_data_bucket,
              'LinuxMinCpus': str(args.linux_min_cores),
              'LinuxMaxCpus': str(args.linux_max_cores),
              'LinuxStartCpus': str(args.linux_start_cores),
              'LinuxClusterAmi': linux_ami,
              'WindowsInstanceType': args.windows_instance_type,
              'WindowsMinInstances': str(args.windows_min_instances),
              'WindowsMaxInstances': str(args.windows_max_instances),
              'WindowsStartInstances': str(args.windows_start_instances)}
    param_cf = [{'ParameterKey': k, 'ParameterValue': v} for k, v in params.iteritems()]
    response = cf_client.create_stack(StackName=args.stack_name,
                                      TemplateURL=args.master_cloudform,
                                      Parameters=param_cf,
                                      Capabilities=['CAPABILITY_NAMED_IAM'],
                                      DisableRollback=args.disable_rollback
                                      )
    stack_arn = response['StackId']
    logger.info("Cloudformation stack ARN: {}".format(stack_arn))

    teardown_out = 'teardown-{}.py'.format(args.stack_name)
    if not args.no_teardown:
        logger.info("Writing teardown script to {}".format(teardown_out))
        with open(teardown_out, 'w') as fout:
            fout.write(teardown_template.format(stack_name=stack_arn,
                                                region_name=args.region_name))

    logger.info("Waiting for stack deployment to complete (this takes ~30 minutes)")
    waiter = cf_client.get_waiter('stack_create_complete')
    # wait up to 45 minutes for stack to build
    waiter.config.delay = 5
    waiter.config.max_attempts = 540
    try:
        waiter.wait(StackName=args.stack_name)
    except WaiterError as we:
        logger.error("It looks like CloudFormation failed: ")
        logger.error(we)
        if args.disable_rollback:
            if args.no_teardown:
                logger.error("--no-teardown and --disable-rollback were specified, so created resources will need to "
                             "be manually removed. This can be done in the AWS console under CloudFormation")
            else:
                logger.error("Cloudformation rollback was disabled, to tear down the partially-created resources, "
                             "run {}".format(teardown_out))
        else:
            logger.error("Cloudformation rollback is enabled, partially created resources will be automatically "
                         "removed by Amazon.")
        return

    logger.info("Cloudformation stack formation successful")
    cf_stack = cloudformation.Stack(stack_arn)
    cf_stack_outputs = {output['OutputKey']: output['OutputValue'] for output in cf_stack.outputs}

    logger.info("=========================")
    logger.info("Params for lakitu config:")
    logger.info("=========================")
    logger.info("windows_cluster_name = {}".format(cf_stack_outputs['WinCluster']))
    logger.info("windows_autoscale_name = {}".format(cf_stack_outputs['WinAutoScalingGroup']))
    logger.info("linux_job_queue_arn = {}".format(cf_stack_outputs['LinuxJobQueueArn']))
    logger.info("log_group_name = {}".format(cf_stack_outputs['LogGroup']))
    logger.info("log_region = {}".format(cf_stack_outputs['LogRegion']))
    logger.info("s3_run_bucket = {}".format(args.s3_data_bucket))
    logger.info("region = {}".format(args.region_name))
    logger.info("task_iam_arn = {}".format(cf_stack_outputs['ECSTaskRoleARN']))


if __name__ == '__main__':
    main()
